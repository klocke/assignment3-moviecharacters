﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MovieCharacters.Models;
using MovieCharacters.Models.DTO;

namespace MovieCharacters.Profiles
{
    /// <summary>
    /// Create Automapper between Domain and DTO
    /// </summary>
    public class FranchiseProfile : Profile
    {

        /// <summary>
        /// Custom mapping for Read: Movies should not be included as objects.
        /// Rather, an integerlist is created to represent the id of movies. 
        /// 
        /// For the other DTS's, the standard mapper is used. 
        /// </summary>
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>().ForMember(fdto => fdto.Movies,
                opt => opt.MapFrom(f => f.Movies.Select(m => m.Id).ToArray())
                ).ReverseMap();

            CreateMap<FranchiseCreateDTO, Franchise>();
            CreateMap<FranchiseEditDTO, Franchise>();

        }
    }
}
