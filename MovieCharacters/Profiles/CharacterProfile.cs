﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MovieCharacters.Models;
using MovieCharacters.Models.DTO;
namespace MovieCharacters.Profiles
{
    /// <summary>
    /// Create Automapper between Domain and DTO
    /// </summary>
    public class CharacterProfile : Profile
    {
        /// <summary>
        /// Custom mapping for Read: Movies should not be included as object.
        /// Rather, an integerlist is created to represent the id of movies. 
        /// 
        /// For the other, the standard mapper is used. 
        /// </summary>
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt.MapFrom(ch => ch.Movies.Select(m => m.Id).ToArray()))
                .ReverseMap();

            CreateMap<CharacterCreateDTO, Character>();
            CreateMap<CharacterEditDTO, Character>();
        }
    }
}
