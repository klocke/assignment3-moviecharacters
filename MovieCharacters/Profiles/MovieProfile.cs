﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MovieCharacters.Models;
using MovieCharacters.Models.DTO;

namespace MovieCharacters.Profiles
{
    /// <summary>
    /// Create Automapper between Domain -- DTO
    /// </summary>
    public class MovieProfile : Profile
    {
        /// <summary>
        /// Custom mapping for Read: Characters are represented as integer array. Franchise as integer. 
        /// 
        /// For the other DTO's, the standard mapper is used. 
        /// </summary>
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>().ForMember(mdto => mdto.Characters, opt => opt.MapFrom(m => m.Characters.Select(ch => ch.Id).ToArray()))
                                            .ForMember(mdto => mdto.Franchise, opt => opt.MapFrom(m=>m.Franchise.Id))
                                            .ReverseMap();

            CreateMap<MovieCreateDTO, Movie>();
            CreateMap<MovieEditDTO, Movie>();


        }
    }
}
