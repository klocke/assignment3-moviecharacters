﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Mime;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using MovieCharacters.Models;
using MovieCharacters.Models.DTO;
using MovieCharacters.Services;

namespace MovieCharacters.Controllers
{
    /// <summary>
    /// Controller for Movie-related Crud and extended methods. 
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        /// <summary>
        /// Constructor. Startup.cs contains the necessary dependency injection for the service
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="movieService"></param>
        public MoviesController(IMapper mapper, IMovieService movieService)
        {
            _mapper = mapper;
            _movieService = movieService;
        }

        /// <summary>
        /// Fetches all Movies in database async. 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMoviesAsync());
        }

        /// <summary>
        /// Fetches a single movie based on id -- if it exists. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _movieService.GetMovieAsync(id);

            if (movie is null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Edit movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dtoMovie"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO dtoMovie)
        {
            if (id != dtoMovie.Id)
            {
                return BadRequest();
            }

            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            await _movieService.UpdateMovieAsync(domainMovie);

            return Ok(domainMovie);
        }

        /// <summary>
        /// Add new movie to database.
        /// </summary>
        /// <param name="dtoMovie"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO dtoMovie)
        {
            Movie movie = _mapper.Map<Movie>(dtoMovie);
            await _movieService.AddMovieAsync(movie);

            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }

        /// <summary>
        /// Delete movie with specified id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            await _movieService.DeleteMovieAsync(id);
            return NoContent();
        }

        /// <summary>
        /// Update characters in a movie by the characters unique id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterIds"></param>
        /// <returns></returns>
        [HttpPut("{id}/characters")]
        public async Task<ActionResult> UpdateCharactersInMovie(int id, int[] characterIds)
        {
            if (!_movieService.MovieExists(id))
            {
                return BadRequest();
            }

            await _movieService.UpdateCharactersInMovie(id, characterIds);

            return NoContent();
        }

        /// <summary>
        /// Fetches all characters in a movie. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersInMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return BadRequest();
            }

            var movie = await _movieService.GetMovieAsync(id);
            if (movie.Characters is null || movie.Characters.Count == 0)
            {
                return NotFound();
            }

            return _mapper.Map<List<CharacterReadDTO>>(movie.Characters);
            //return movie.Characters.Select(ch => _mapper.Map<CharacterReadDTO>(ch)).ToArray(); // alternative
        }
    }
}
