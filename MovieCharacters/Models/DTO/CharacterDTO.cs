﻿using System.Collections.Generic;

/* Data transfer objects for Characters. 
 * Generally, I suspect inheritance here is not a good idea, 
 * but for the present case I simply found it cleaner. 
 */

namespace MovieCharacters.Models.DTO
{

    /// <summary>
    /// DTO for creating a new character in database.
    /// </summary>
    public class CharacterCreateDTO
    {
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Gender { get; set; }
    }

    /// <summary>
    /// Editing aldready existing characters require Id as well.
    /// </summary>
    public class CharacterEditDTO : CharacterCreateDTO
    {
        public int Id { get; set; }

    }

    /// <summary>
    /// Reading shoul also include associated movies. 
    /// </summary>
    public class CharacterReadDTO : CharacterEditDTO
    {
        public List<int> Movies { get; set; }
    }
}
