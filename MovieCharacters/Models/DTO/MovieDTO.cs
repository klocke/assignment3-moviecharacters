﻿using System;
using System.Collections.Generic;


/* Data transfer objects for Movie. 
 * Generally, I suspect inheritance here is not a good idea, 
 * but for the present case I simply found it cleaner. 
 */

namespace MovieCharacters.Models.DTO
{
    /// <summary>
    /// DTO for creating a new Movie in database.
    /// </summary>
    public class MovieCreateDTO
    {
        public string Title { get; set; }

        public string Genre { get; set; }

        public int ReleaseYear { get; set; }

        public string Director { get; set; }
        public Uri Picture { get; set; }
        public Uri Trailer { get; set; }
    }

    /// <summary>
    ///  Editing requires Id
    /// </summary>
    public class MovieEditDTO : MovieCreateDTO
    {
        public int Id { get; set; }

    }

    /// <summary>
    /// Reading characters should include all movies (by Id) and the FranchiseId.
    /// </summary>
    public class MovieReadDTO : MovieEditDTO
    {
        public List<int> Characters { get; set; }
        public int Franchise { get; set; }
    }
}
