﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/* Data transfer objects for Franchise. 
 * Generally, I suspect inheritance here is not a good idea, 
 * but for the present case I simply found it cleaner. 
 */

namespace MovieCharacters.Models.DTO
{
    /// <summary>
    /// DTO for creating a new franchise in database.
    /// </summary>
    public class FranchiseCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }

    /// <summary>
    /// Edit DTO. requires ID
    /// </summary>
    public class FranchiseEditDTO : FranchiseCreateDTO
    {
        public int Id { get; set; }
    }

    /// <summary>
    /// Reading should include all Movies in the franchise (by Id, not the entire object)
    /// </summary>
    public class FranchiseReadDTO : FranchiseEditDTO
    {
        public List<int> Movies { get; set; }
    }
}
