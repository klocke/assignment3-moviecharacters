using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using System.Collections;

namespace MovieCharacters.Models
{   
    /// <summary>
    /// Domain Character class.
    /// </summary>
    [Table("Character")]
    public class Character
    {
        /// <summary>
        /// Identity of the Character. Used as primary key in database
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of character. Cannot exceed 50 characters. 
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        /// <summary>
        /// Alias of character. Not required
        /// </summary>
        [MaxLength(50)]
        public string Alias { get; set; }

        /// <summary>
        /// The Characters gender. Not required. 
        /// </summary>
        [MaxLength(50)]
        public string Gender { get; set; }

        /// <summary>
        /// Link to a picture representation of the character. 
        /// </summary>
        public Uri Picture { get; set; }


        // many-to-many relationship with movies

        /// <summary>
        /// Movies the character appears in.
        /// </summary>
        public ICollection<Movie> Movies { get; set; }
    }

}