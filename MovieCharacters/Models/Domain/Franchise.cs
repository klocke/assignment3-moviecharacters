using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;


namespace MovieCharacters.Models
{
    /// <summary>
    /// Domain Franchise class
    /// </summary>
    [Table("Franchise")]
    public class Franchise
    {
        /// <summary>
        /// Unique Id of franchise. Used as primary key in Franchise table.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of franchise
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Name { get; set;}

        /// <summary>
        /// Description of franchise
        /// </summary>
        public string Description { get; set;}
        
        
        /// <summary>
        /// 1:N Relationship with Movies. One franchise consists of many movies. 
        /// </summary>
        public ICollection<Movie> Movies { get; set; }


    }

}