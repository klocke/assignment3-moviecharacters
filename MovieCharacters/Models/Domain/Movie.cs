using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;



namespace MovieCharacters.Models
{
    /// <summary>
    /// Domain Movie class. 
    /// </summary>
    [Table("Movie")]
    public class Movie
    {
        /// <summary>
        /// Unique Id -> used as PK in database.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Title of movie
        /// </summary>
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }

        /// <summary>
        /// Genre of movie.
        /// </summary>
        public string Genre { get; set; }

        /// <summary>
        /// Releaseyear of movie.
        /// </summary>
        public int ReleaseYear { get; set; }

        /// <summary>
        /// Name of person who directed this movie. 
        /// </summary>
        [MaxLength(50)]
        public string Director { get; set; }

        /// <summary>
        /// Link to picture representation of movie.
        /// </summary>
        public Uri Picture { get; set; }

        /// <summary>
        /// Link to movie trailer
        /// </summary>
        public Uri Trailer { get; set; }


        // Relationships

        /// <summary>
        /// Many-to-many relationship with Characters
        /// </summary>
        public ICollection<Character> Characters { get; set; }

        /// <summary>
        /// Many to one relationship with franchise. Uses FranchiseId as foreign key
        /// </summary>
        public int? FranchiseID { get; set; }

        /// <summary>
        /// Franchise object. 
        /// </summary>
        public Franchise Franchise { get; set; }


    }
}