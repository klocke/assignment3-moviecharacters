using MovieCharacters.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacters.Models
{
    /// <summary>
    /// DatabaseContext.
    /// </summary>
    public class MovieCharactersDbContext : DbContext
    {
        /// <summary>
        /// Defines the Character Table.
        /// </summary>
        public DbSet<Character> Characters { get; set; }

        /// <summary>
        /// Defines the Franchise Table
        /// </summary>
        public DbSet<Franchise> Franchises { get; set; }

        /// <summary>
        /// Defines the Movies Table
        /// </summary>
        public DbSet<Movie> Movies { get; set; }

        /// <summary>
        /// Constructor. No custom options.
        /// </summary>
        /// <param name="options"></param>
        public MovieCharactersDbContext([NotNull] DbContextOptions options) : base(options) { }

        /// <summary>
        /// Configure database before creation. Dependency injection ensures the sql connectionstring
        /// is passed from appsettings.json
        /// </summary>
        /// <param name="optionsBuilder"></param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }

        /// <summary>
        /// Specifies how the database is to be constructed. 
        /// Includes some seeded data for testing purposes
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            // Seed data




            // Franchises

            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 1,
                Name = "Marvel Cinematic Universe",
            });
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 2,
                Name = "James Bond"
            });
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 3,
                Name = "Harry Potter"
            });




            // Movies
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 1,
                Title = "Kill Bill: Volume 1",
                Genre = "Action",
                Director = "Quentin Tarantino",
                ReleaseYear = 2003,
                Trailer = new Uri("https://www.youtube.com/watch?v=c_dNIXwrbzY")
            });


            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 2,
                Title = "Deadpool",
                Genre = "Action, Comedy",
                Director = "Tim Miller",
                ReleaseYear = 2016,
                FranchiseID = 1
            });


            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 3,
                Title = "Another Round",
                ReleaseYear = 2020,
                Director = "Thomas Vinterberg",
                Trailer = new Uri("https://www.imdb.com/video/vi4070162713/?"),
                Genre = "Comedy, Drama"
            });

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 4,
                Title = "Harry Potter and the Order of the Phoenix",
                Director = "David Yates",
                ReleaseYear = 2007,
                FranchiseID = 3
            });

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 5,
                Title = "Iron Man",
                Genre = "Action, Comedy",
                Director = "Jon Favreau",
                ReleaseYear = 2008,
                FranchiseID = 1
            });

            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 6,
                Title = "Thor",
                Genre = "Action, Comedy",
                Director = "Kenneth Branagh",
                ReleaseYear = 2008,
                FranchiseID = 1
            });



            // Characters
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 1,
                Name = "Jack Sparrow",
                Gender = "male",
                Picture = new Uri("https://no.wikipedia.org/wiki/Jack_Sparrow#/media/Fil:Johnny_Depp_as_Captain_Jack_Sparrow_in_Queensland,_Australia.jpg")
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 2,
                Name = "Gandalf",
                Gender = "male",
                Alias = "The Gray"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 3,
                Name = "Hermione Granger",
                Gender = "female"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 4,
                Name = "Iron Man",
                Gender = "male"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 5,
                Name = "Thor",
                Gender = "male"
            });




            // N:N relationship.
            // Creates a table "CharacterMovie" which contains MovieId and CharacterId, linking the two tables.
            // Some seeded links.
            modelBuilder.Entity<Character>()
                .HasMany(c => c.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                "CharacterMovie",
                l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                mc =>
                {
                    mc.HasKey("CharacterId", "MovieId");
                    mc.HasData(new
                    {
                        CharacterId = 4,
                        MovieId = 5
                    },
                        new
                        {
                            CharacterId = 5,
                            MovieId = 6
                        },
                        new
                        {
                            CharacterId = 3,
                            MovieId = 4
                        });


                });

        }
    }
}