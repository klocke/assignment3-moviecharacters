using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MovieCharacters.Models;

namespace MovieCharacters.Services
{

    /// <summary>
    /// CharacterService: Injected as dependency in startup of our app.
    /// </summary>
    public class CharacterService : ICharacterService
    {
        private readonly MovieCharactersDbContext _context;


        /// <summary>
        /// Constructor: Takes dbcontext as input. 
        /// </summary>
        /// <param name="context"></param>
        public CharacterService(MovieCharactersDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Character>> GetAllCharactersAsync()
        {
            return await _context.Characters.Include(f => f.Movies).ToListAsync();
        }
        public async Task<Character> GetCharacterAsync(int id)
        {
            return await _context.Characters.FindAsync(id);
        }
        public async Task<Character> AddCharacterAsync(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;

        }
        public async Task UpdateCharacterAsync(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task DeleteCharacterAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
        }
        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(f => f.Id == id);
        }
    }
}