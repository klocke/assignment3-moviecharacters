using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MovieCharacters.Models;

namespace MovieCharacters.Services
{
    /// <summary>
    /// Interface for Franchise service. 
    /// Defines necessary CRUD-operations and some extra functionality.
    /// </summary>
    public interface IFranchiseService
    {
        // CRUD - These methods are self-explanatory
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        public Task<Franchise> GetFranchiseAsync(int id);
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        public Task UpdateFranchiseAsync(Franchise franchise);
        public Task DeleteFranchiseAsync(int id);
        public bool FranchiseExists(int id);

        // Extras
        /// <summary>
        /// Fetches all movies in a given franchise. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<IEnumerable<Movie>> GetMoviesInFranchise(int id);

        /// <summary>
        /// Fetches all (unique) characters appearing in a given franchise. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<IEnumerable<Character>> GetCharactersInFranchise(int id);

    }
}