using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MovieCharacters.Models;

namespace MovieCharacters.Services
{
    /// <summary>
    /// Interface for CharacterService: Contains methods for character-related functionality
    /// </summary>
    public interface ICharacterService
    {

        /// <summary>
        /// Method for fetching all characters in database asynchronoounsosoously
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Character>> GetAllCharactersAsync();

        /// <summary>
        /// Method for fetching a single character specified by id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Character> GetCharacterAsync(int id);

        /// <summary>
        /// Add character.
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public Task<Character> AddCharacterAsync(Character character);

        /// <summary>
        /// Update character
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public Task UpdateCharacterAsync(Character character);

        /// <summary>
        /// Delete character
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteCharacterAsync(int id);

        /// <summary>
        /// Check if character exists in DB
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CharacterExists(int id);
    }
}