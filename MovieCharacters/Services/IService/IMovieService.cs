﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using MovieCharacters.Models;

namespace MovieCharacters.Services
{
    /// <summary>
    /// Interface for movie service. 
    /// </summary>
    public interface IMovieService
    {
        // General CRUD
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        public Task<Movie> GetMovieAsync(int id);
        public Task<Movie> AddMovieAsync(Movie movie);
        public Task UpdateMovieAsync(Movie movie);

        public Task DeleteMovieAsync(int id);
        public bool MovieExists(int id);

        // Extra functionality
        /// <summary>
        /// replaces the characters in a movie with a new list of characters based on their unique id. 
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="characterIds"></param>
        /// <returns></returns>
        public Task UpdateCharactersInMovie(int movieId, int[] characterIds);
    }
}