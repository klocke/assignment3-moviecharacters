﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using MovieCharacters.Models;

namespace MovieCharacters.Services
{
    /// <summary>
    /// Implementation of interface.
    /// Injected as dependency in the app. 
    /// </summary>
    public class MovieService : IMovieService
    {


        private readonly MovieCharactersDbContext _context;

        public MovieService(MovieCharactersDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies.Include(m => m.Characters).ToListAsync();
        }
        public async Task<Movie> GetMovieAsync(int id)
        {
            return await _context.Movies.Include(m=>m.Characters).FirstOrDefaultAsync(m=>m.Id ==id);
        }
        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }
        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }
        
        
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(m => m.Id == id);
        }
        
        /// <summary>
        /// First finds all characters in list, then adds these to the linking table CharacterMovie.
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="characterIds"></param>
        /// <returns></returns>
        public async Task UpdateCharactersInMovie(int movieId, int[] characterIds)
        {
            var characters = characterIds.Select(id =>  _context.Characters.Find(id)).ToList();
            var movie = await _context.Movies.FindAsync(movieId);
            movie.Characters = characters;
            await UpdateMovieAsync(movie); // _context.SaveChanges() is called in UpdateMovieAsync
        }

    }
}