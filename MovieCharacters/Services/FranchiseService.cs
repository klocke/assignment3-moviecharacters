using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using MovieCharacters.Models;

namespace MovieCharacters.Services
{
    /// <summary>
    /// Implements functionality documented in IFranchiseService
    /// </summary>
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieCharactersDbContext _context;

        public FranchiseService(MovieCharactersDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises.Include(f => f.Movies).ToListAsync();
        }
        public async Task<Franchise> GetFranchiseAsync(int id)
        {
            return await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);
        }
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;

        }
        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
        }
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(f => f.Id == id);
        }

        
        public async Task<IEnumerable<Movie>> GetMoviesInFranchise(int id)
        {
            var franchise = await GetFranchiseAsync(id);
            return franchise?.Movies; // Avoid nullreferenceobject

        }

        /// <summary>
        /// First finds all characters in a franchise, then selects only unique characters based on their id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Character>> GetCharactersInFranchise(int id)
        {

            var franchise =await _context.Franchises.Include(f => f.Movies)
                                                    .ThenInclude(m => m.Characters)
                                                    .FirstOrDefaultAsync(f=>f.Id == id);


            return  franchise.Movies.SelectMany(m => m.Characters).GroupBy(c => c.Id).Select(group => group.First());
            //return franchise.Movies.SelectMany(m => m.Characters).Distinct(); // Can implement IEqualityComparer

        }
    }
}