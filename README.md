# MovieCharacter API

Simple ASP.NET Core Web API app keeping track of movies, moviecharacters, and franchises for which these movies belong. 


Ensure the correct connectionstring is added to `appsettings.json`.

## Structure

* `Models/` contain both `domain` and `DTO` models. Also `DbContext`
* `Controllers/` Are where the endpoints of the api, and handles all calls by calling methods defined in the services and mapping these to/from `domain` <-> `DTO`
* `Services/` contains business logic for the domain models. The dependency of these services are injected in `startup.cs`
* `Profiles/` contain the automapper definitions. 